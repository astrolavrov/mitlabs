$(document).ready(function () {

  //burger click
  $('#js-burger').on('click', toggleMenu);

  //close main menu
  $('#js-close').on('click', toggleMenu);

  //фенсибокс
  $('[data-fancybox]').fancybox();

  //custom file-input
  $('.f-file-input').on('change', function() {
    let file = $(this)[0].files[0].name;
    $(this).prev('.f-file').text(file);
  });

  //расчитать стоимость
  $('#js-calculate').on('click', function(event) {
    let form = $('#js-calculate-form'),
        position = $(this).offset().top + $(this).height();

    form.slideDown(300);
    $('html, body').animate({scrollTop: position},500);
    event.preventDefault();
  });

  //табы
  $('.b-tabs .tab-item').on('click', function (event) {
    let currentId = $(this).data('id'),
        tabsList = $(this).parents('.tabs-list').find('.tab-item'),
        containers = $(this).parents('.b-tabs').find('.tab-content'),
        mobileTab = $(this).parents('.tabs-holder').find('.mobile-tab'),
        listHolder = $(this).parents('.tabs-holder').find('.tabs-list'),
        currentText = $(this).text();

    tabsList.removeClass('active');
    $(this).addClass('active');
    containers.hide();
    $('#js-' + currentId).fadeIn(150);
    mobileTab.text(currentText).removeClass('open');
    listHolder.removeClass('show');
    event.preventDefault();
  });

  //мобильные табы
  $('.mobile-tab').on('click', function() {
    let tabsList = $(this).parents('.tabs-holder').find('.tabs-list');

    $(this).toggleClass('open');
    tabsList.toggleClass('show');
  });
});

let toggleMenu = function toggleMenu(event) {
  let menu = $('.b-navigation');
  menu.toggleClass('active');
  event.preventDefault();
};

$(document).mouseup(function(e) {
  let menu = $('.b-navigation');

  if (!menu.is(e.target) && menu.has(e.target).length === 0) {
    menu.removeClass('active');
  }
});
